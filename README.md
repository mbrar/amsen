Maheep Brar 

This is the code for my personal autonomous robotics project, designed to autonomously navigate and map out its surroundings while simultaneously collecting environmental data
and overlaying it onto the same map.

NOTE: The project is still in progress. The code on GitLab is currently out of date, will be updated soon.